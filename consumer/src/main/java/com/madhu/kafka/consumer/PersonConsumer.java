package com.madhu.kafka.consumer;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.databind.node.POJONode;
import com.madhu.model.Person;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;

import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

/**
 * Created by madalasa on 2/17/17.
 */
public class PersonConsumer {

    public static void main(String[] args) throws JsonProcessingException {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("group.id", UUID.randomUUID().toString());
//        props.put("group.id", "testgroup1");
        props.put("enable.auto.commit", "true");
        props.put("auto.offset.reset", "earliest");
        props.put("auto.commit.interval.ms", "1000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.connect.json.JsonDeserializer");
        KafkaConsumer<String, JsonNode> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList("person-topic"));
        while (true) {
            ConsumerRecords<String, JsonNode> records = consumer.poll(100);
            ObjectMapper objectMapper = new ObjectMapper();
            for (ConsumerRecord<String, JsonNode> record : records) {

                System.out.printf("offset = %d, key = %s, value = %s%n", record.offset(), record.key(), record.value());
                Person p = objectMapper.treeToValue(record.value(), Person.class);
                System.out.println("Person:"+p);
            }
        }
    }
}
