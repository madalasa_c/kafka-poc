import java.util.ArrayList;
import java.util.List;

/**
 * Created by madalasa on 2/21/17.
 */
public class Sample {

    public static void processList(List<? extends TestInterface> list){
        list.stream().forEach(el -> System.out.println(el));
    }

    public static void processListBounded(List<TestInterface> list){
        list.stream().forEach(el -> System.out.println(el));
    }


    public static <T> void processT(List<T> list){
        list.stream().forEach(el -> System.out.println(el));
    }


    public static void processUnbounded(List<?> list){
        list.stream().forEach(el -> System.out.println(el));
    }

    public static void main(String[] args) {
        List<ClassB> classBS = new ArrayList<>();
        classBS.add(new ClassB());
        processList(classBS);
        List<TestInterface> testInterfaces = new ArrayList<>();
        testInterfaces.add(new ClassA());
        testInterfaces.add(new ClassB());

        processListBounded(testInterfaces);

        processT(testInterfaces);

        processUnbounded(testInterfaces);
    }
}
