package com.madhu.kafka.producer;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.POJONode;
import com.madhu.model.Person;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Properties;

/**
 * Created by madalasa on 2/16/17.
 */
public class PersonProducer {

    public static void main(String[] args) throws IOException {
        Properties props = new Properties();
        props.put("bootstrap.servers", "localhost:9092");
        props.put("acks", "all");
        props.put("retries", 0);
        props.put("batch.size", 16384);
        props.put("linger.ms", 1);
        props.put("buffer.memory", 33554432);
        props.put("key.serializer", "org.apache.kafka.common.serialization.StringSerializer");
//        props.put("value.serializer", "org.apache.kafka.common.serialization.BytesSerializer");
//        Producer<String, Bytes> producer = new KafkaProducer<>(props);
        props.put("value.serializer", "org.apache.kafka.connect.json.JsonSerializer");
        Producer<String, JsonNode> producer = new KafkaProducer<>(props);

        int i=0;

        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

        while (true){
            System.out.println("Enter your message");
            String str = br.readLine();
            Person person = new Person(str, 2, 73292333);
//            producer.send(new ProducerRecord<String, Bytes>("my-replicated-topic", Integer.toString(i), new Bytes(p.toString().getBytes())));

            producer.send(new ProducerRecord<String, JsonNode>("person-topic", Integer.toString(i), new POJONode(person)));
//            i++;
        }
    }
}
