package com.madhu.kafka.streams;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.KeyValue;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.KStreamBuilder;

import java.util.Properties;

/**
 * Created by madalasa on 2/26/17.
 */
public class NameStream {

    public static void main(String[] args) {
        String sinkTopic = "full-name";
        Properties streamsConfiguration = new Properties();
        streamsConfiguration.put(StreamsConfig.APPLICATION_ID_CONFIG, "names-example");
        streamsConfiguration.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
        streamsConfiguration.put(StreamsConfig.KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        streamsConfiguration.put(StreamsConfig.VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass().getName());
        KStreamBuilder kStreamBuilder = new KStreamBuilder();
        KStream<String, String> names = kStreamBuilder.stream(Serdes.String(), Serdes.String(), "fname-topic", "lname-topic");

        names.foreach((key, value) -> System.out.println("key:"+key+", value:"+value));


//        names.mapValues(value -> value.concat("fromstream")).to(sinkTopic);
        names.groupByKey()
             .reduce((value1, value2) -> value1+","+value2, "full-name").to(sinkTopic);

//        names.to("full-name");
        KafkaStreams kafkaStreams = new KafkaStreams(kStreamBuilder, streamsConfiguration);
        kafkaStreams.start();
    }
}
