package com.madhu.kafka.streams;

import org.apache.kafka.streams.processor.Processor;
import org.apache.kafka.streams.processor.ProcessorContext;

/**
 * Created by madalasa on 2/25/17.
 */
public class NameProcessor implements Processor<String, String> {

    @Override
    public void init(ProcessorContext context) {

    }

    @Override
    public void process(String key, String value) {

    }

    @Override
    public void punctuate(long timestamp) {

    }

    @Override
    public void close() {

    }
}
