package com.madhu.model;

import java.io.Serializable;

/**
 * Created by madalasa on 2/23/17.
 */
public class Person implements Serializable{

    private String name;
    private int age;
    private int phone;

    public Person(String name, int age, int phone) {
        this.name = name;
        this.age = age;
        this.phone = phone;
    }

    public Person() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "{name:"+name+",age:"+age+",phone:"+phone+"}";
    }
}
